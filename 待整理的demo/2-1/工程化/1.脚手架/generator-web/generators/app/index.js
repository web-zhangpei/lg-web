// 入口文件

// 生成器父类
const Generator = require('yeoman-generator')

// 生成器子类
module.exports = class extends Generator {
  // 钩子函数（声明周期）
  wrigting() {
    // 往项目目录写入方法
    this.fs.write(this.destinationPath('temp.txt'), Math.random().toString())
  }

  prompting() {
    // 用户提示
    this.log(
      yosay(
        `Welcome to the cool ${chalk.red('generator-xxx-porject')} generator!`
      )
    )
    return this.prompt([{
        type: 'input',
        name: 'name',
        message: '你的项目名称',
  
        //   默认项目生成目录名
        default: this.appname,
      },{}]).then((answers) => {
      // 用户输入结果， {name:'user input value'}
      // 挂载到类对象上面，以便writing使用
      this.answers = answers
    })
  }

  
}
