# my-module [![NPM version][npm-image]][npm-url] [![Build Status][travis-image]][travis-url] [![Dependency Status][daviddm-image]][daviddm-url]
> module,node

## Installation

```sh
$ npm install --save my-module
```

## Usage

```js
const myModule = require('my-module');

myModule('Rainbow');
```
## License

MIT © [张培](https://gitee.com/codeByPei/a_web_test)


[npm-image]: https://badge.fury.io/js/my-module.svg
[npm-url]: https://npmjs.org/package/my-module
[travis-image]: https://travis-ci.com/peterZhang66/my-module.svg?branch=master
[travis-url]: https://travis-ci.com/peterZhang66/my-module
[daviddm-image]: https://david-dm.org/peterZhang66/my-module.svg?theme=shields.io
[daviddm-url]: https://david-dm.org/peterZhang66/my-module
