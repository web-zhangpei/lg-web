// 扩展名mjs
import { name } from "./module.mjs";
console.log(name);
// 实验阶段特性 node --experimental-modules index.mjs

// 支持加载原生模块
import fs from 'fs';
fs.writeFileSync('./readme.txt','es module')

// 支持第三方模块
import _ from 'lodash';
console.log(_.camelCase('es module'))

// 原生模块兼容了具名导出
import {writeFileSync} from 'fs';
// 三方模块没兼容具名导出
import {camelCase} from 'lodash';
