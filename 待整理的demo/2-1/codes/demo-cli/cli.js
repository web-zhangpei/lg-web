#!/usr/bin/env node
 
// yarn add inquirer --dev
const inquirer = require("inquirer")
const fs = require('fs')
const path = require('path')
const ejs = require('ejs')

inquirer.prompt([
    {
        // 输入方式
        type:'input',
        // 问题返回结果的键名
        name:'name',
        // 给用户的提示
        message:'project name？'
    }
]).then(answers => {
    // console.log(answers) --> {name:'aaa'}
    // 模板目录
    const tmplDir = path.join(__dirname,'templates') //--> 项目模板目录
    // 生成目录
    const desDir = process.cwd() //--> 根目录
    // console.log(tmplDir,desDir)
    // --> E:\work\desktopFiles\a_web_test\fed-e-task-01-03\codes\demo-cli\templates
    // --> E:\work\desktopFiles\a_web_test\fed-e-task-01-03\codes\demo-cli

    // fs 读取目录文件
    fs.readdir(tmplDir,(err,files) => {
        console.log(files) //--> 文件名数组[ 'index.css', 'index.html' ]
        // 
        if (err) throw err
        files.forEach(file => {
            // file 单个文件名
            ejs.renderFile(path.join(tmplDir,file),answers,(err,result) =>{
                if (err) throw err
                // console.log(result) --> 各个文件内容
                // 将结果写入各个目录
                fs.writeFileSync(path.join(desDir,file),result)
            })
        })
    })
})