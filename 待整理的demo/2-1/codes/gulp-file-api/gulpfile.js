const { src, dest } = require("gulp");
const cleanCss = require("gulp-clean-css");
const rename = require("gulp-rename");

exports.default = () => {
  // 读取、写入
  //    return src('index.css').pipe(dest('dest'))
  // 通配多文件
  // return src('*.css').pipe(dest('dist'))
  // 压缩插件 gulp-clean-css
  return src("*.css")
    .pipe(cleanCss())
    .pipe(rename({ extname: ".min.css" }))
    .pipe(dest("dist"));
  // gulp-rename
};
