// grunt入口文件
// 用于定义一些需要grunt自动执行的任务
// 需要导出一个函数
// 此函数接收一个grunt的形参，内部提供一些api,可以快速创建构建任务。
const sass = require("sass");
const loadGruntTasks = require("load-grunt-tasks");
module.exports = (grunt) => {
  // 多个任务
  grunt.registerTask("foo", () => {
    console.log("grunt");
    // 获取配置
    console.log(grunt.config("foo"));
    // 支持点语法
    console.log(grunt.config("foo.number"));
  });
  //
  grunt.registerTask("bar", "任务描述", () => {
    console.log("bar 任务描述");
  });
  //grunt 不支持异步
  //   grunt.registerTask("async-task", () => {
  //     setTimeout(() => {
  //       console.log("async-task");
  //     }, 1000);
  //   });
  // this.async()
  grunt.registerTask("async-task", function () {
    const done = this.async();
    setTimeout(() => {
      console.log("async-task");
      done(false);
    }, 1000);
  });
  // 标记任务失败
  grunt.registerTask("bad", () => {
    var a = 1;
    if (a === 1) {
      return false;
    }
  });
  // 默认任务
  // 串行
  //   grunt.registerTask("default", ["foo","bad", "async-task","bar"]);
  grunt.initConfig({
    foo: {
      number: 123,
    },
  });
  //   grunt.registerTask("default", ["foo"]);

  //  多目标任务
  grunt.registerMultiTask("fns", function () {
    console.log("multi task"); //  No "fns" targets found. 没有给fns设置多个target
    // this拿到target和目标数据data
    // 指定的键都会成为一个目标target，值都成为一个数据data
    console.log(`target:${this.target},data:${this.data}`);
    // 子任务获取options
    console.log(`options:${JSON.stringify(this.options())}`);
  });
  // 多目标任务配置多个目标
  grunt.initConfig({
    // 配置选项,会作为每个任务的配置出现。
    // 每个任务也可以单独配置选项，并覆盖总配置。
    fns: {
      options: {
        name: "peter",
        age: "27",
      },
      target1: "目标1",
      target2: "目标2",
      html: "html文件",
      js: "js文件",
      obj: {
        options: {
          name: "aaa",
        },
      },
    },
    clean: {
      // temp1:'temp/app.js',
      // 通配符
      // temp2:'temp/*.txt',
      // 所有子目录及其文件(目录文件也会删除)
      temp3: "temp/**",
    },
    // 配置sass目标
    sass: {
      options: {
        //   实现的模块
        implementation: sass,
        // 资源地图
        sourceMap: true,
      },
      main: {
        files: {
          "dist/css/main.css": "src/scss/main.scss",
        },
      },
    },
    babel: {
      options: {
        // 指定转换哪些特性
        presets:['@babel/preset-env']
      },
      main: {
        files: {
          "dist/js/app.js": "src/js/app.js",
        },
      }, 
    },
    watch:{
        js:{
            files:['src/js/*.js'],
            // 文件改变需要执行什么任务
            tasks:['babel']
        },
        css:{
            files:['src/scss/*.scss'],
            tasks:['sass']
        }
    }
  });

  //  使用加载插件
  // 多目标任务
  //   grunt.loadNpmTasks("grunt-sass");
  //The implementation option must be passed to the Sass task --> 必须有implementation实现选项
  loadGruntTasks(grunt);
//   启动先执行编译
  grunt.registerTask('default',['sass','babel','watch'])
};
