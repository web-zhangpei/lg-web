// 此文件作文generator核心入口文件
// 需要导出一个继承自yeoman generator 的类型
// yeoman generator 在工作时会自动调用我们在此类型中定义的一些生命周期方法
// 在这些方法中可以通过调用父类提供的一些工具方法实现一些功能，比如文件写入。

const Generator = require("yeoman-generator");

module.exports = class extends Generator {
  // 上下文提示
  prompting() {
    return this.prompt({
      type: "input",
      name: "name",
      message: "你的项目名称",
      //   默认项目生成目录名
      default: this.appname,
    }).then(answers => {
        // 用户输入结果， {name:'user input value'}
        // 挂载到类对象上面，以便writing使用
        this.answers = answers
    });
  }
  //往项目目录写入方法
  writing() {
    //yeoman自动在生成文件阶段调用此方法
    //fs是内部封装的，不同于node的fs
    //两个参数，绝对路径和文件内容
    // this.fs.write(this.destinationPath('temp.txt'),Math.random().toString())

    // 三个参数
    // const tmpl = this.templatePath("foo.txt");
    const tmpl = this.templatePath("bar.html");
    // const output = this.destinationPath("foo.txt");
    const output = this.destinationPath("bar.html");
    // 模板数据上下文
    // const context = { title: "hello zp", success: false };
    const context = this.answers;
    this.fs.copyTpl(tmpl, output, context);
  }
};
