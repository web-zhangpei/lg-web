//gulp的入口文件

const GulpClient = require("gulp");

//运行在node环境，可以用common.js规范
exports.foo = (done) => {
  console.log("工作");
  // The following tasks did not complete: foo 任务没有完成
  // [19:19:11] Did you forget to signal async completion? 是否忘记表示接受1.
  //gulp取消同步代码模式，约定每个任务都必须是异步的任务。任务执行过后，需要执行回调函数或者其他方式去标记这个任务已经完成。
  //   done是个回调函数
  done(); //标识任务完成。
};

// 默认任务
// exports.default = (done) => {
//   console.log("default");
//   done();
// };
//gulp4.0之后版本保留这个api
// 不被推荐，推荐使用exports
GulpClient.task("bar", (done) => {
  //   console.log("bar");
  //   done();

  setTimeout(() => {
    console.log("bar");
    done();
  }, 1000);
});

// 组合任务 两个api
const { series, parallel } = require("gulp"); // 连续  并行
const task1 = (done) => {
  setTimeout(() => {
    console.log("task1");
    done();
  }, 1000);
};
const task2 = (done) => {
  setTimeout(() => {
    console.log("task2");
    done();
  }, 1000);
};
const task3 = (done) => {
  setTimeout(() => {
    console.log("task3");
    done();
  }, 1000);
};
// 串行 先编译后部署的流程
// exports.default = series(task1,task2,task3)
// 并行 同时编译css、js等不同类型文件
// exports.default = parallel(task1, task2, task3);

// 三种方式  gulp错误优先
// 默认方式
//promise (gulp会忽略resolve传入的参数)
exports.promise = () => {
  // 可以
  //   return Promise.resolve();
  //   不可以
  //  new Promise((res) => {
  //     console.log("promise task");
  //     res();
  //   });
  //   可以
  //   return new Promise((res) => {
  //     console.log("promise task");
  //     res();
  //   });
  //   不可以
  //  new Promise((res) => {
  //   console.log("promise task");
  //   return res();
  // });
};
exports.promise_err = () => {
  return Promise.reject(new Error("task failed"));
};
//async (需要node版本高于8)
const timeout = (time) => {
  return new Promise((resolve, reject) => {
    // setTimeout(resolve,time)
    setTimeout(() => {
      //    resolve()    --> 可以
      //   return resolve() --> 可以通知gulp异步结束
      //   reject(new Error("task failed")); --> 可以通知gulp异步结束
      //   return reject(new Error("task failed")); --> 可以通知gulp异步结束
    });
  });
};
exports.async = async () => {
  await timeout(1000);
  console.log("async task");
};
// stream 流 异步事件
const fs = require("fs");
exports.stream = (done) => {
  // 可读流
  const readStream = fs.createReadStream("package.json");
  const writeStream = fs.createWriteStream("temp.txt");
  readStream.pipe(writeStream);
  // 模拟了end事件  根据流的状态判断是否完成
  // return readStream
  // 还原end
  //   写入流监测不到end?
  readStream.on("end", () => {
    done();
  });
};
