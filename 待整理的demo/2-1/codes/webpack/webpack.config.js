const path = require("path");
console.log(path.join(__dirname, "output"));
module.exports = {
  mode: "none",
  // 入口路径，相对路径./不能省略
  entry: "./src/index.css",
  output: {
    filename: "index.js",
    // path必须绝对路径
    path: path.join(__dirname, "dist"),
  },
  //   开发过程辅助工具
  devtool: "source-map",
  //   其他资源模块加载规则
  module: {
    rules: [
      {
        test: /.css$/,
        //   从后往前使用，
        // css-loader 打包成js模块
        //style-loader 将转化厚的模块转成style标签
        use: ["style-loader", "css-loader"],
      },
    ],
  },
};
