const fs = require('fs');
const {Transform} = require("stream");
const { chunk } = require('lodash');


exports.default = () => {
    // 文件读取流
    const read = fs.createReadStream('index.css');
    // 文件写入流
    const write = fs.createWriteStream('index.min.css');
    // 文件转换流
    const transform = new Transform({
        transform:(chunk,encoding,callback) => {
            // 核心转换代码实现
            // chunk => 读取流中读取到的内容（ buffer )
            const input = chunk.toString()
            // 替换空白字符  替换注释
            const output  = input.replace(/\s+/g,'').replace(/\/\*.+?\*\//g,'');
            // 错误优先函数 第一个参数应该传入错误对象，没有错误传入null
            callback(null,output)
        }
    })

    // 把读取出的文件流导入写入文件流
    read.pipe(transform).pipe(write)
    // 根据读取流的状态判断是否玩成
    return read;
}