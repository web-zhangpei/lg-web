## 1.谈谈你对工程化的初步认识，结合你之前遇到过的问题说出三个以上工程化能够解决问题或者带来的价值。
- 初步认识
    - 工程化可以提高团队的开发效率，约束项目规范和风格；所有为了提高效率、减少成本的手段都可以叫做工程化。webpack不等于工程化，只是一个脚手架工具。
- 价值
    - 可以帮我们构建项目基础代码，规范代码风格和目录结果；
    - 可以帮我们去处理一些手动重复的低效行为，比如：特性编译、代码压缩打包、项目构建、项目部署。
    - 可以轻松切换生产环境和开发环境。
## 2.你认为脚手架除了为我们创建项目结构，还有什么更深的意义？
- 统一团队的代码规范，保证代码质量；
- 提高团队开发效率；
## 3. 概述脚手架实现的过程，并使用NOdejs完成一个自定义的小型脚手架工具
编写入口文件；结合结合命令行交互问题的结果，用指定目录下面的模板生产项目的结构。
代码见/codes/demo-cli
## 4.使用gulp完成项目的自动化构建
- 实现思路：
    - 编写gulp的入口文件；
    - 用对应插件分别写html/css/js的编译任务；
    - 开发阶段用serve任务进行编译、热加载打开项目,编辑结果放在临时目录temp；
    - 打包上线build,需要对图片压缩、代码压缩、资源路径修改、资源合并；然后把结果输出到dist目录下面；
- 代码见/codes/gulp-day-stream;
## 5.使用grunt完成项目的自动化构建
- 实现思路：
    - 编写grunt的入口文件；
    - 用对应插件分别写html/css/js的编译任务；
    - 开发阶段用serve任务进行编译、热加载打开项目,编辑结果放在临时目录temp；
    - 打包上线build,需要对图片压缩、代码压缩、资源路径修改、资源合并；然后把结果输出到dist目录下面；
- 代码见/codes/grunt;

