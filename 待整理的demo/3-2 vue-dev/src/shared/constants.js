export const SSR_ATTR = 'data-server-rendered'

// vue静态成员
export const ASSET_TYPES = [
  'component',
  'directive',
  'filter'
]

// vue生命周期
export const LIFECYCLE_HOOKS = [
  'beforeCreate',
  'created',
  'beforeMount',
  'mounted',
  'beforeUpdate',
  'updated',
  'beforeDestroy',
  'destroyed',
  'activated',
  'deactivated',
  'errorCaptured',
  'serverPrefetch'
]