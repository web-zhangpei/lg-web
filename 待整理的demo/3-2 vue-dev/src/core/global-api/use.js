/* @flow */

import { toArray } from '../util/index'

export function initUse (Vue: GlobalAPI) {
  // 函数入参：对象或函数
  Vue.use = function (plugin: Function | Object) {
    // 插件列表
    const installedPlugins = (this._installedPlugins || (this._installedPlugins = []))
    // 有插件说明已注册
    if (installedPlugins.indexOf(plugin) > -1) {
      return this
    }

    // additional parameters
    // 调用install方法，并传递相应参数
    // 去除数组第一个元素（plugin）
    const args = toArray(arguments, 1)
    // 把this(Vue)插入第一个元素位置
    args.unshift(this)
    if (typeof plugin.install === 'function') {
      plugin.install.apply(plugin, args)
    } else if (typeof plugin === 'function') {
      plugin.apply(null, args)
    }
    // 保存已安装插件，返回Vue类
    installedPlugins.push(plugin)
    return this
  }
}
// 问题怎么判断插件已安装
