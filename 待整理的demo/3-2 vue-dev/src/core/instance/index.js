import { initMixin } from './init'
import { stateMixin } from './state'
import { renderMixin } from './render'
import { eventsMixin } from './events'
import { lifecycleMixin } from './lifecycle'
import { warn } from '../util/index'

function Vue (options) {
  if (process.env.NODE_ENV !== 'production' &&
    !(this instanceof Vue)
  ) {
    // 不是生产环境 && 不是vue实例（当做普通函数调用时）
    warn('Vue is a constructor and should be called with the `new` keyword')
  }
  this._init(options) 
}
// 注册vm的_init(),初始化vm  --- 混入（添加原型方法）
initMixin(Vue)
stateMixin(Vue)
eventsMixin(Vue)
lifecycleMixin(Vue)
renderMixin(Vue)

export default Vue
