const path = require('path')
const webpack = require('webpack')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const VueLoaderPlugin = require('vue-loader/lib/plugin')
const CopyWebpackPlugin = require('copy-webpack-plugin')

/* 
webpack
webpack-cli
*/
/* 
loaders:
vue-loader vue-template-compiler
style-loader css-loader
*/
/* 
plugins:
html-webpack-plugin
*/
module.exports = {
  // 入口路径，相对路径./不能省略
  entry: {
    app: './src/main.js',
  },
  output: {
    // mode:"development",
    filename: '[name].bundle.js',
    // path必须绝对路径
    path: path.join(__dirname, 'dist'),
  },

  devServer: {
    //使用观察者模式(启动一个服务器，当文件有变动时，页面立即改变)
    // contentBase: './public', //将dist目录设置为可访问文件
    compress: true, //一切服务都启用gzip 压缩
    port: 9000, //指定服务器监听的端口 8080：为默认端口
    // hotonly: true, //告诉服务器，正在使用模块热替换
  },
  plugins: [
    new HtmlWebpackPlugin({
      title: '这个是html模板',
      template: './public/index.html', //配置html模板
      inject: true, //是否自动在模板文件添加 自动生成的js文件链接
      minify: {
        removeComments: true, //是否压缩时 去除注释
      },
    }),
    new webpack.DefinePlugin({
      BASE_URL: JSON.stringify('./public/'),
    }),
    new webpack.NamedModulesPlugin(), //模块热替换相关
    new webpack.HotModuleReplacementPlugin(), //模块热替换相关
    new VueLoaderPlugin(),
    new CopyWebpackPlugin({
      patterns: [
        {
          from: './public/*.ico',
          to: path.join(__dirname, 'dist'),
        },
      ],
    }),
  ],
  module: {
    rules: [
      {
        test: /\.vue$/,
        exclude: /node_modules/,
        use: ['vue-loader'],
      },
      {
        test: /\.css$/,
        use: ['style-loader', 'css-loader'],
      },
      {
        test: /\.less$/,
        use: ['style-loader', 'css-loader', 'less-loader'],
      },
      {
        //加载图片(js或css中引入图片时)
        test: /\.(png|svg|jpg|gif)$/,
        use: [
          {
            loader: 'file-loader',
            options: {
              esModule: false,
              name: '[path][name].[ext]', //配置自定义文件模板
              outputPath: '.', //配置打包后的输出目录(.代表在dist目录中生成)
            },
          },
        ],
      },
    ],
  },
}
