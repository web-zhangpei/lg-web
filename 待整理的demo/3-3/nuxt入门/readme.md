### 三种使用方式
- 初始项目
- nuxt中间件 - 集成在node server
- 现有vue项目改造 - 10%以上     

### 案例代码分支说明
1. git init --- 生成.git文件
2. 加.gitignore --- 忽略的文件
3. git status --- 查看git跟踪文件状态
4. git add . --- 添加所有文件
5. git commit -m '' --- 提交日志
6. git push
7. git branch 查看分支
8. git branch name  新建分支
9. git checkout name 切换分支
10. git checkout -b name 新建 + 切换分支 

## nuxt入门
- 路由
    1. 基本路由 --- 固定路径生成
    2. 自定义路由
    3. 嵌套路由
    4. 动态路由
    5. 路由导航
    6. 编程式路由
- 视图
    1. 模板
    2. 布局
- 数据
    1. 异步数据 asyncData
    2. 上下文对象


    
01-base-router
02-router
master