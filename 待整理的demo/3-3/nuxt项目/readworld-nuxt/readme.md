**项目介绍**

- [git仓库](https://github.com/gothinkster/realworld)
- [实例地址](http://demo.realworld.io/#/)
- [页面模板](https://github.com/gothinkster/realworld-starter-kit/blob/master/FRONTEND_INSTRUCTIONS.md)
- [接口地址](https://github.com/gothinkster/realworld/tree/master/api)
- [nuxt中文文档](https://zh.nuxtjs.org/docs/2.x/configuration-glossary/configuration-components)
- [免费cdn网站](https://www.jsdelivr.com/)
- [部署](https://nuxtjs.org/docs/2.x/get-started/commands)
- 


**初始化项目**

- mkdir readworld-nuxt
- npm init -y
- cnpm i nuxt
- package.json --- "dev":"nuxt"
- pages/index.vue
- 导入样式资源
    - 本地化：免费cdn网站 ---  ionicons --- 对应版本号

**开始开发**
- 自定义路由规则
- 导入所有页面
- 处理页面导航链接
- 路由高亮
    - home 需要精确匹配 exact
- 封装请求模块
    - npm i axios
**业务开发**
- 登录注册业务
- 接口封装抽离，统一维护
- 表单校验
- 注册接口
- 登录信息共享
    - store/index.js
    - 页面访问权限  --- 路由中间件
- 文章列表
    - 跳个人页
    - 跳文章详情页
    - 点赞
    - 文章分页
    - 文章标签
    - 并行请求
- 文章详情


**部署**
链接到服务端
- ssh root@39.105.28.5
- FQGC9Kn/eye1W8icdBgrQp+KkGYoFgbVr17bmjey0Wc.

**知识迷惑点**
- routes.splice(0) js清空
- arr.push()  可以传参数序列，都会被按顺序放入数组中
- @submit.prevent="" 阻止默认行为
- Auth External API(JWT) 身份验证外部API