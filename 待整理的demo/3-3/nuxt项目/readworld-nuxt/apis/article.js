import {
    request
} from '@/plugins/request'

// 公共文章列表
export const getArticles = params => {
    return request({
        method: 'GET',
        url: '/api/articles',
        // 注意：传入的字段名是params,不是data
        params
    })
}
// 用户文章列表
export const getUserArticles = params => {
    return request({
        method: 'GET',
        url: '/api/articles/feed',
        // 注意：传入的字段名是params,不是data
        params
    })
}
// 文章标签列表
export const getTags = data => {
    return request({
        method: 'GET',
        url: '/api/tags',
    })
}
// 添加点赞
export const addFavorite = slug => {
  return request({
    method: 'POST',
    url: `/api/articles/${slug}/favorite`
  })
}

// 取消点赞
export const deleteFavorite = slug => {
  return request({
    method: 'DELETE',
    url: `/api/articles/${slug}/favorite`
  })
}
// 作者主页详情
export const getProfile = username => {
    return request({
      method: 'GET',
      url: `/api/profiles/${username}`
    })
  }
  // 文章详情
  export const getArticle = slug => {
      return request({
        method: 'GET',
        url: `/api/articles/${slug}`
      })
    }