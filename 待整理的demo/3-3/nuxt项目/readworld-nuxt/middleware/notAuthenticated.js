// notAuthenticated.js  没有认证 --- 没有认证页面的中间件
export default function({
    store,
    redirect
}) {
    if (store.state.user) {
        console.log('已登录去首页')
        return redirect('/')
    }
}