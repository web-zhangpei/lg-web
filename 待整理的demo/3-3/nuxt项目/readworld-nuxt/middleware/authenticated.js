/* 
middleware/authenticated.js
验证是否登录
*/
// 导出一个匿名函数
export default function ({
    store,
    redirect
}) {
    // 注意：路由导航之前，中间件不打印、不debug，因为在服务端渲染
    console.log('未登录去登录页')
    if (!store.state.user) {

        return redirect('/login')
    }
}