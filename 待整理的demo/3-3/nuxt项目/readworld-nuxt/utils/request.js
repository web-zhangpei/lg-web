/* 
基于axios请求模块
*/
import axios from 'axios';
const request = axios.create({
    baseURL: 'https://conduit.productionready.io'
    // baseURL: '/api'
})

// 请求拦截器


// 响应拦截器


// 业务处理

export default request