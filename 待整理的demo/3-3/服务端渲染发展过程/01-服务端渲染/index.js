// 框架
const express = require('express')
const fs = require('fs')
const template = require('art-template')
// app
const app = express()

// get数据
const data = JSON.parse(fs.readFileSync('./data.json'))
const tpl = fs.readFileSync('./index.html', 'utf-8')

// 组装页面
/* const page = template.render('hello {{msg}}', {
    msg: 'art-template'
}) */
const page = template.render(tpl, data)

// 路由
app.get('/', (req, res) => {
    // 返回页面
    // res.send('hello world ~')
    // get模板
    // console.log(tpl)
    // console.log(data)
    // res.send(tpl)
    // res.end(tpl)
    res.end(page)
})

// 监听
app.listen(3000, () => console.log('running'))

// 启动命令
// npm install -g nodemon
// nodemon 文件名

// 模板引擎
// cnpm i art-template
// 语法 
