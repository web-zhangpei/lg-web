/* 
三、基于以下代码完成下面四道题

*/
const fp = require("lodash/fp");
// 数据
// horse_power 马力,dollar_value 价格,in_srock 库存
const cars = [
  {
    name: "Ferrari FF",
    horse_power: 660,
    dollar_value: 70000,
    in_srock: true,
  },
  {
    name: "Audi R8",
    horse_power: 525,
    dollar_value: 114200,
    in_srock: false,
  },
];
/* 
    练习1：使用函数组合fp.flowRight()重新是此案下面这个函数

    let isLastInStock = function (cars) {
    // 获取最后一条数据
    let last_car = fp.last(cars)
    // 获取最后一条数据的 in_stock 属性值
    return fp.prop('in_stock',last_car)
}
*/

let fn1 = fp.flowRight(fp.prop("dollar_value"), fp.last);
let result = fn1(cars);
console.log(result);

/* 
    练习2：使用fp.flowRight()、fp.prop()和fp.first()获取第一个car的name
*/
let fn2 = fp.flowRight(fp.prop("name"), fp.first);
console.log(fn2(cars));

/* 
    练习3：使用帮助函数_average重构 averageDollarValue ,使用函数组合的方式实现
*/
let _average = function (xs) {
  // 累加器
  return fp.reduce(fp.add, 0, xs) / xs.length;
}; // < - 无需改动
let averageDollarValue = function (cars) {
  let dollar_values = fp.map(function (car) {
    return car.dollar_value;
  }, cars);
  return _average(dollar_values);
};
let fn3 = fp.flowRight(
  _average,
  fp.map(function (car) {
    return car.dollar_value;
  })
);
console.log(fn3(cars));
/* 
  练习4：使用flowRight写一个sanitizeNames()函数，返回一个下划线连接的小写字符串，把数组中的name转换为这种形式：例如:
  sanitizeNames(['Hellow World']) =>["hello_word"];
*/
let _underScore = fp.replace(/\W+/g, "_");
let fn4 = fp.flowRight(
  fp.map(item => fp.flowRight(_underScore, fp.toLower)(item))
);
console.log(fn4(["Hellow World"]));
