/* 
二、下面代码用promise改进

*/
// 源代码
setTimeout(function () {
  var a = "hello";
  setTimeout(function () {
    var b = "lagou";
    setTimeout(function () {
      var c = "i o u";
      console.log(a + b + c);
    }, 10);
  }, 10);
}, 10);
// 优化后
function p1(a) {
  return new Promise((resolve) => {
    setTimeout(function () {
      resolve(a);
    });
  });
}
function p2(a, b) {
  return new Promise((resolve) => {
    setTimeout(function () {
      resolve(a + b);
    });
  });
}

p1("hello")
  .then((a) => {
    return p2(a, "lagou");
  })
  .then((res) => {
    var c = "i o u";
    console.log(res + c);
  });
