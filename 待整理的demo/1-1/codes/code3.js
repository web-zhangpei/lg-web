/* 
    三、基于以下代码完成下面四道题

*/ const fp = require("lodash/fp");
// support.js
class Container {
  static of(value) {
    return new Container(value);
  }
  constructor(value) {
    this._value = value;
  }
  map(fn) {
    return Container.of(fn(this._value));
  }
}

class Maybe {
  static of(x) {
    return new Maybe(x);
  }
  isNothing() {
    return this._value === null || this._value === undefined;
  }
  constructor(x) {
    this._value = x;
  }
  map(fn) {
    return this.isNothing() ? this : Maybe.of(fn(this._value));
  }
}
/* 
    练习1：使用fp.add(x,y)和fp.map(f,x)创建一个能让functor里的值增加的函数ex1
*/
let exl = () => {
  // 需要实现的函数
  return Maybe.of([5, 6, 1]).map((arr) => {
    return fp.map((item) => fp.add(10, item), arr);
  });
};
console.log(exl()._value);
/* 
    练习2：实现一个函数ex2，能够使用fp.fisrt获取列表的第一个元素
*/
let xs = Container.of(["do", "ray", "me"]);
let ex2 = () => {
  return xs.map((arr) => {
    return fp.first(arr);
  });
};
console.log(ex2()._value);

/* 
    练习3：实现一个函数ex3,使用safeProp和fp.first找到user的名字的首字母。
*/
let safeProp = fp.curry(function (x, o) {
  return Maybe.of(o[x]);
});
let user = { id: 2, name: "peter" };
let ex3 = () => {
  return safeProp("name")(user).map((name) => fp.first(name));
};
console.log(ex3()._value);
/* 
    练习4：使用Maybe重写ex4,不要有if语句
*/
let ex4 = function (n) {
//   if (n) {
//     return parseInt(n);
//   }
  return Maybe.of(n).map((val) => parseInt(val));
};
console.log(ex4(3.3)._value);
console.log(ex4()._value);

