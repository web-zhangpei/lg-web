## 一、vue 项目实战

### 1. 创建组件的三种方式：

1. vue.extend() + options API
2. ClassAPI
3. ClassAPI +  decorator  (装饰器decorator方便，但是没通过草案，不稳定)

教育管理后台

cli 安装

- yarn global add @vue/cli 版本 3.0
- 对于 Vue 3，您应该使用 Vue CLI v4.5，
  使用升级命令：vue upgrade --next
- 使用 vite 创建 vue3
  - npm init @vitejs/app <project-name>
    yarn create @vitejs/app <project-name>

![image-20210412063458827](..\images\image-20210412063458827.png)

![image-20210412072401731](..\images\image-20210412072401731.png)

![image-20210412072643748](..\images\image-20210412072643748.png)

![image-20210412072757317](..\images\image-20210412072757317.png)

![image-20210412073512934](..\images\image-20210412073512934.png)

严谨的提示：字符串类型只提示字符串方法。

#### class 风格：装饰器介绍

还是草案阶段。 --- 阮一峰《es6 入门》

![image-20210412074423560](..\images\image-20210412074423560.png)

### 2. 代码格式规范：

![image-20210412215533977](D:\Users\张培\Desktop\gitee\lg-web\3 vue源码与进阶\images\image-20210412215533977.png)

1-比较简单的规范；2-爱彼迎规范比较严谨；3-google的一种规范。
http://standardjs.com   http://github.com/airbnb/javascript   

![image-20210412220249355](D:\Users\张培\Desktop\gitee\lg-web\3 vue源码与进阶\images\image-20210412220249355.png)

代码校验工具eslint  + 代码格式规范starndard

![image-20210412222215910](D:\Users\张培\Desktop\gitee\lg-web\3 vue源码与进阶\images\image-20210412222215910.png)

自定义校验规则：

配置的套路和思路：
找到对应的校验插件  ---  package.json插件悬浮官网   ---  找rules中的配置规则。以下图为例

![image-20210412225817446](D:\Users\张培\Desktop\gitee\lg-web\3 vue源码与进阶\images\image-20210412225817446.png)

### 3. 样式处理

![image-20210413060831943](D:\Users\张培\Desktop\gitee\lg-web\3 vue源码与进阶\images\image-20210413060831943.png)

#### 共享全局样式变量：

每个页面都要引入

![image-20210413062807128](D:\Users\张培\Desktop\gitee\lg-web\3 vue源码与进阶\images\image-20210413062807128.png)

向所有组件注入样式

![image-20210413063400248](D:\Users\张培\Desktop\gitee\lg-web\3 vue源码与进阶\images\image-20210413063400248.png)

### 4. 接口处理

接口地址：
http://eduboss.lagou.com/boss/doc.html#/home
http://113.31.105.128/front/doc.html#/home
官网地址：
http://edubss/lagou.com/#/login

![image-20210413070432501](D:\Users\张培\Desktop\gitee\lg-web\3 vue源码与进阶\images\image-20210413070432501.png)

测试是否代理成功：localhost:8080 + /boss, 返回有响应（Unauthorized）

### 5. 页面布局

![image-20210413072613354](D:\Users\张培\Desktop\gitee\lg-web\3 vue源码与进阶\images\image-20210413072613354.png)

### 6. 登录

1. 接口测试（postman）
2. 请求登录（form数据格式 :npm i qs；设置contentType；去axios官网查看）
3. 处理请求结果
4. 请求时禁用登录 （loading:true）
5. 表单验证(查看async-validator更多使用)
6. 封装请求方法
7. 请求体data和contentType问题（常用三种）

![image-20210414063041531](D:\Users\张培\Desktop\gitee\日程管理v0.3.5\assets\images\image-20210414063041531.png)

![image-20210414064752318](D:\Users\张培\Desktop\gitee\日程管理v0.3.5\assets\images\image-20210414064752318.png)

import {Form} from ' element'  引入Form类型声明，好处是智能提示更精准



![image-20210414065453573](D:\Users\张培\Desktop\gitee\日程管理v0.3.5\assets\images\image-20210414065453573.png)

总结: 

```js
/**
     * 不同类型的data,对应的Content-Type
     * 1. 普通对象 --- application/json(axios post 默认)
     * 2. qs.stringify(data) --- application/x-www-form-urlencoded(key=value&key=value)
     * 3. FormData 对象 --- multipart/form-data
     */
```

![image-20210414072758029](D:\Users\张培\Desktop\gitee\日程管理v0.3.5\assets\images\image-20210414072758029.png)

技巧：postman统一添加请求头

### 几个待处理的问题：

1. 登录后，不能跳登录；
2. token过期处理
3. 三五分钟token --- 

### 7. 用户登录和身份认证

处理token过期

- 概念介绍
- 分析响应拦截器
- axios错误处理
- 错误消息提示
- 实现基本流程逻辑
- 关于多次请求问题
- 解决多次请求刷新token问题
- 解决多次请求接口重试的问题

![image-20210414223650220](D:\Users\张培\Desktop\gitee\日程管理v0.3.5\assets\images\image-20210414223650220.png)

关于token：过期时间、过期处理
两种方式：请求前，响应后（荐）

无痛刷新token

![image-20210416061454939](D:\Users\张培\Desktop\gitee\lg-web\3 vue源码与进阶\images\image-20210416061454939.png)

![image-20210416063647357](D:\Users\张培\Desktop\gitee\lg-web\3 vue源码与进阶\images\image-20210416063647357.png)

```js
let notSure: any = 4
notSure = 'maybe it is a string'
notSure = 'boolean'
// 在任意值上访问任何属性都是允许的：
notSure.myName
// 也允许调用任何方法：
notSure.getName()
```

![image-20210416073430527](D:\Users\张培\Desktop\gitee\lg-web\3 vue源码与进阶\images\image-20210416073430527.png)

封装上传图片组件，通过参数让组件更加灵猴。

### 8. 课程管理

### 9. 添加课程

#### 富文本编辑器

推荐：
wangeditor.com
ckeditor5（公司做的，不错）
quill
medium-editor(最近更新少了)
wangEditor(个人团队)
ueditor(百度，不再维护)
tinymce

![image-20210417185149694](D:\Users\张培\Desktop\gitee\lg-web\3 vue源码与进阶\images\image-20210417185149694.png)

vue语法技巧：
默认值是false,声明属性就会为true
开发技巧：
开发前罗列要做的功能，todo
做一步，去vuetools看数据；
脏数据处理可以先写死一个值测试。

### 10.  上传课程视频

阿里云视频点播服务介绍

![image-20210418061510255](D:\Users\张培\Desktop\gitee\lg-web\3 vue源码与进阶\images\image-20210418061510255.png)

![image-20210418061943335](D:\Users\张培\Desktop\gitee\lg-web\3 vue源码与进阶\images\image-20210418061943335.png)

开发快捷技巧：win + v，打开剪切板列表

### 11. 面包屑处理

1. 基于rbac的前后端权限管理方案
   - 动态路由
   - 权限比对
2. 纯前端