# 一、基础回顾

目录结构

1. react介绍
2. jsx语法
   1. jsx中表达式
   2. jsx中属性
   3. 标记必须闭合
   4. className
   5. 自动展开数组
   6. 三元运算
   7. 循环
   8. 事件
   9. 样式
   10. ref属性
3. 组件（类组件、函数组件）
4. 表单（受控表单、非受控表单）
5. 路由

## 3. 组件

[七个组件的生命周期函数](https://blog.csdn.net/weixin_43851769/article/details/88188325)

## 5.1 路由

npm i react-router-dom

### 5.1.1 基本使用

![image-20210421214257007](D:\Users\张培\Desktop\gitee\lg-web\4 react原理与实战\images\image-20210421214257007.png)

### 5.1.2 路由嵌套

![image-20210421214936141](D:\Users\张培\Desktop\gitee\lg-web\4 react原理与实战\images\image-20210421214936141.png)

### 5.1.3 路由传参



![image-20210421215800313](D:\Users\张培\Desktop\gitee\lg-web\4 react原理与实战\images\image-20210421215800313.png)

### 5.1.4 路由重定向

![image-20210421220051413](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20210421220051413.png)

# 二、VDOM与Diff算法

精简版react

启动 npm start

## jsx到底是什么

js语法扩展，被用来描述用户界面（更轻松来编写）

jsx转换过程

jsx ---- React.createElement() --- Vdom --- 真实dom

## 什么是VDOM

dom对象的js对象表现	

### VDOM如何提升效率

最核心就是最小化更新，只更新需要更新的节点。

创建VDOM

babel会将jsx转为react元素。加行注释：/**@jsx TinyReact.createElement*/

![image-20210422062222470](D:\Users\张培\Desktop\gitee\lg-web\4 react原理与实战\images\image-20210422062222470.png)

这种方法每个文件都要，可以全局配置babel

![image-20210422062411494](D:\Users\张培\Desktop\gitee\lg-web\4 react原理与实战\images\image-20210422062411494.png)

pragma:TinyReact.createElement(编译指示，杂注)

### VDOM比对更新

1. 节点类型不相同（不用比对，直接更新）
2. 节点相同的情况（props.type相同）
   - 同级比较：父与父，子与子。
   - 深度优先：子优先于父。

![image-20210424071510389](D:\Users\张培\Desktop\gitee\lg-web\4 react原理与实战\images\image-20210424071510389.png)

### 删除节点

- 更新完之后再删除
- 保证在同一个父节点的所有子节点里
- 删除条件：旧节点数量多于新vdom

![image-20210424091041158](D:\Users\张培\Desktop\gitee\lg-web\4 react原理与实战\images\image-20210424091041158.png)



### 类组件状态更新

	1. component组件内部静态方法：setState/getDom/setDom/
	2. 通过Component,使用setDom，拿到旧节点

### 组件更新

1. 不是同一组件，直接用mountElement（）将返回的vdom添加到页面中。
2. 是同一组件，最新props传入得到最新vdom，新增方法diffComponent()对比差异，更新组件。
3. [组件生命周期函数的实现](https://blog.csdn.net/zy21131437/article/details/107611565)
4. [结合生命周期做性能优化](https://blog.csdn.net/Luzahngfeng/article/details/106068970)

#### 核心功能函数树

- render
  - diff
    - diffComponent
      - mountElement
        - mountComponent  --- 最终都会成为普通元素，用mountNativeElement处理
        - mountNativeElement

### ref属性实现（TODO）

实现思路

- 创建节点判断有无ref属性
- 创建出的dom作为参数，传入ref存储的方法，调用方法

两种情况

1. 普通元素
2. 类组件或函数组件

### key属性：

key局部唯一（同一父节点），不用全局唯一
key最好不能变化，不建议用下标

#### 节点对比

实现思路：
旧dom元素，用key遍历存入对象keyElements里
vdom去查找，找不到添加；找到了比对

#### 节点删除

# 三、Fiber

开发环境配置

npm install webpack webpack-cli webpack-node-externals @babel/core @babel/preset-env @babel/preset-react babel-loader nodemon npm-run-all -D
npm install express

![image-20210425061625500](D:\Users\张培\Desktop\gitee\lg-web\4 react原理与实战\images\image-20210425061625500.png)

  "start":"npm run all --paraller dev:*"   执行所有dev开头命令（paraller 平行运行）

#### 原生API之requestIdleCallback

```js
requestIdleCallback((deadline) =>{//空余时间 
//const doneFlag =  deadline.timeRemaining() > 1 
})
```

##### 空闲时间

一秒60帧，页面流畅不卡顿。一帧？毫秒。



#### fiber的产生背景

之前比对的diff算法，用的stack(循环加递归)，主线程长期被占用，用户行为不及时造成卡顿。
再加上js单线程，递归不能中断，无法同时执行其他任务。
这是react16之前的问题。

#### filber的解决方案：

浏览器空闲时间执行任务，不会长时间占用主线程。
放弃递归，采用可被中断的循环。
任务拆分成一个个小任务（fiber 纤维）

#### fiber的实现思路：

为实现任务的终止再继续，拆分两部分：
虚拟dom比对，可被终止。（构建fiber）
真实dom更新，不可被终止。(提交commit)

![image-20210502221956460](D:\Users\张培\Desktop\gitee\lg-web\4 react原理与实战\images\image-20210502221956460.png)

每个节点、第一个子节点、第一子节点的兄弟节点（父、长子、二弟、三弟；二叔、长堂兄、二堂兄）

#### 初始化与更新过程：

初始化渲染：vdom --> Fiber --> Fiber[] --> dom
更新：newFiber vs oldFiber -->Fiber[] --> dom

#### 接下来

- 任务队列创建、添加
- 实现任务的调度（空闲）
- 构建fiber对象 - 根节点
- 构建fiber对象 - 子节点
- 完善fiber对象 - stateNode属性、tag属性
- 构建fiiber对象 - 剩余子节点
- 构建fiiber对象 - 同级节点（从下往上）
- 构建fiiber对象 - effects数组（放所有fiber）
- 初始dom渲染
- 类组件处理
- 函数组件处理
- 实现更新节点