# es新特性

意义：总体来讲，每次技术的进步，都带来了更优的编程体验。

- let、const、块级作用域
- 对象结构、数组结构
- 模板字符串
- 参数默认值、剩余参数
- 展开数组
- 箭头函数与this
- 对象字面量增强
- Object方法新增：Object.assign、Object.is
- Proxy、defineProperty、Reflect(统一操作对象API 13个)
- promise
- class类、静态方法、类的继承
- Set、Map、Symbol
- for of循环
- 可迭代接口、迭代器模式
- 生成器、生成器应用
- es Modules

专题介绍（promise/esModules）

## 1. 聊聊 ECMAScript

语言和平台：es 和 js
es是js标准化规范，js是es扩展语言
es只提供了基本语法，js的扩展：浏览器操作dom、node读写文件

web浏览器环境下：![image-20210508060319559](D:\Users\张培\Desktop\gitee\lg-web\1js深度剖析\images\image-20210508060319559.png)
node环境下：
![image-20210508060720492](D:\Users\张培\Desktop\gitee\lg-web\1js深度剖析\images\image-20210508060720492.png)
平台提供的内置模块和api

es版本发布时间：
![image-20210508060900728](D:\Users\张培\Desktop\gitee\lg-web\1js深度剖析\images\image-20210508060900728.png)
es5之后，不再用版本号命名。统一用年份命名。es6泛指es2015（含）之后所有的新标准。
有些资料es6只指es2015
建议过一下[es6完整的语言规范](https://262.ecma-international.org/6.0/)

## 2.es6新语法归类

四类：
解决原有语法上的问题（如let/const）
对原有语法进行增强（解构，增强，模板字符串）
全新的对象、方法、功能（promsie/Object.assign()）
全新的数据类型和数据结构（symbol/Set/Map）

## 1.作用域增强：let/const/块级作用域

es6之前两种作用域：
全局作用域
函数作用域
块级作用域（新增）

es6之前1种声明变量关键字：
var
let（新增）
const（新增）

块：花括号{}包裹起来的代码范围
之前块外面可访问块内，可以随意篡改，非常不安全。

let的3个特点：
不能声明提升；
不能重复声明；
会创建一个块级作用域

const的3个特点:
声明一个常量，必须有值；
值不能被修改
会创建一个块级作用域

解构：
数组解构
对象结果

## 2. 函数增强

参数：默认值、剩余参数

函数简写

## 3.数据读写增强

proxy和Object.defineProperty()对比：
1.后者只能监视属性的读、写
2.前者更强大，除读、写属性，还有对象方法的操作、删除
3.对数组操作的更好监视（方法），用后者实现同样功能很麻烦。
4.前者以非侵入方式监管对象的读写（不操作原对象，操作代理对象）
后者会改变原对象，增加很多额外的属性。

![image-20210509202804668](D:\Users\张培\Desktop\gitee\lg-web\1js深度剖析\images\image-20210509202804668.png)
比较常用就3个：get/set/deletePropery

## 4.API规范增强：Reflect

Reflect的好处：
1.它是proxy所有操作对象方法的默认实现；
2.提供了一套统一操作对象属性的API(相较于杂乱的操作方式：delete、in操作符;Object的方法；或者是赋值符= )

## 5.异步编程增强

## 6.类语法增强

实例方法 vs 静态方法
2.基于原型系统模拟类（理解类的实现，编程可以按照类的思想）
3.实例方法的原理：原型对象添加方法
4.静态方法的原理：直接给构造函数添加方法
		静态方法语法：关键字 static 声明
		 静态方法this指向构造函数，不指向实例对象

类的继承 extends
1.类的继承的原理：new构造函数生成的一个新对象，也叫实例对象

## 7.全新的数据结构

Set数据结构
特性：不允许重复
理解：类数组集合
方法：add()、has()、delete(val)、clear()
遍历：forEach/for...of...
size属性
应用场景：数组去重

```js
const arr = [1,2,3,3,4,4,5]
setArr = Array.from(new Set(setArr)) //法1
setArr = [...new Set(setArr)] //法2
```

Map数据结构
特性：键值对集合，键可以是任意类型。类对象集合。
方法：get()/set()/clear()/delete()/has()
遍历：forEach
应用场景：需要用对象作为key等

Symbol数据类型
特性：唯一标识
方法：Symbol.for() 维护了一个全局的Symbol和字符串的对应表。
属性：提供了很多内置常量。作为方法标识符，可以实现内置对象接口：优势：被遍历（for...in.../Object.keys）、字符串序列化(JSON.stringify())忽略掉Symbol属性。特别适合作为对象的私有属性。
场景：
	修改三方模块，扩展其属性，不会覆盖同名的键；
	避免属性名重复产生的问题;
	模拟定义对象的私有成员；
	之前都是规定（下划线私有，按模块标识），只是最大程度规避问题，并没有解决问题。而symbol可以。

```js
console.log(Symbol('a') === Symbol('a')) //false
console.log(Symbol.for('b') === Symbol.for('b')) //true
Object.getOwnPropertySymbols() // 拿到所有symbol属性名
Object.keys() // 拿到所有非symbol属性名
```

## 8.遍历增强

for...of...
优势：break终止遍历;forEach()无法终止;soem()/every()可以终止。
使用前提：
	必须实现了iterable接口 ，es2015则提供了iterable接口（可迭代接口）
	越来越多的数据结构（甚至可自定义），统一了遍历方式
对比：
	for：针对数组
	for...in...：针对键值对
	函数式的遍历方法：针对数组、集合、对象等。
	for...of...：遍历所有数据结构统一方式

![image-20210509224705958](D:\Users\张培\Desktop\gitee\lg-web\1js深度剖析\images\image-20210509224705958.png)
for...of...遍历工作原理，可以用while实现相同过程。

![image-20210509225409122](D:\Users\张培\Desktop\gitee\lg-web\1js深度剖析\images\image-20210509225409122.png)

iterabtor、iterable、iterableResult三者

迭代器模式 ：不用关心复杂多变的数据结构，也能拿到迭代数据。

9.新增步进式执行：

生成器函数
特点：惰性执行，调用一下next()执行一下。

es2016  
1.arr.includes
	旧版 indexOf 不能查找NaN
2.指数运算
	旧版：Math.pow(2,10)
	新版：2 **10  运算符新增

es2017
Object扩展方法：
1.values()
2.entries() 把对象转为双重数组，给map使用 
3.Object.getOwnPropertyDescriptor()
	问题背景：es2015有了getter/setter，但是assign不能复制完整的对象。它就可以获取完整的对象信息，再重定义一个对象。
4.str.padStart/str.padEnd  给字符串用给定的值，填充前后位置。
5.尾逗号
6.async/await：promise语法糖







# TypeScript

## 语言类型概述

js是弱类型语言，给代码增加了不确定性。js的自有类型系统存在的问题。
ts是js的超集，扩展了类型系统，大大提高代码的可靠程度。

### 1.强类型 vs 弱类型（类型安全）

区别：是否允许类型隐式转换
前者不允许任何数据类型隐式转换。（弱类型允许）

### 2.静态类型 vs 动态类型（类型检查）

区别：是否允许随时修改变量的类型。
前者给变量声明类型之后，就不允许修改。
后者在运行阶段才能确认变量类型。而且变量类型可以随时变化。
![image-20210510064147805](D:\Users\张培\Desktop\gitee\lg-web\1-2 js深度剖析\images\image-20210510064147805.png)
不同类型的语言

#### 弱类型的问题

js：无类型系统、任性、不靠谱
大规模应用下，优势变成了短板。
只有编译阶段，才能发现代码中的类型异常。
![image-20210510065327448](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20210510065327448.png)
问题举例

以往解决这些问题只靠君子约定，有隐患。只有强制要求才有保障。

#### 强类型的优势：

四点：
错误更早暴露（提前消灭一大部分错误）
代码更智能，编码更准确。
重构更可靠。
减少大量的不必要的类型判断。（只写更有意义的代码）

## flow

### 1.介绍与使用

概述：js的类型检查器
使用方式：添加类型注解
开始使用：
	yarn add flow-bin --dev
	文件开头注释：//@flow
	有flow校验，就不需要js校验。关闭vscode自带的。
	yarn flow init 初始化flow配置文件
	yarn flow stop 结束flow服务
移除类型注解两种方案：
	1.flow-remove-types 官方模块，最简单
		yarn flow remove types <指定目录> -d dist
		-d指定移除后的输出目录。
	2.babel
		yarn add @babel/core @babel/cli @babel/preset-flow --dev
		.babelrc文件配置babel预设模块preset-flow
		yarn babel src -d dist
开发工具插件：
	1.问题背景：不直观，控制台查看问题
	2.安装一下

### 2.相关特性

- 类型推断（智能的推断所需要的类型）

- 类型注解
  - 变量类型
  - 形参类型
  - 函数返回值类型
  
- 不同数据类型的注解方式
  - js原有六种原始类型
  - 两种有结构的数据类型：对象、数组
    - 数组
      - 两种注解方式
        - :Array<number>泛型 
        -  :[]
      - 固定长度（叫元组）
        - 注解方式：:[number,string]
      
    - 对象
      - 两种注解方式
        - :{foo:string,bar:number}
          可选属性{foo?:string}
        - 索引器方式：:{[string]:string}
  - 函数类型（特殊数据类型）
    - 两种注解方式
      - 箭头函数式的函数签名，限制形参和返回值
        callback:(string) => void
      - fn(a1:string):void{}
  - flow扩展类型
    1. 联合类型
       多种变量值：const value:'val1' | 'val2'
       多种变量类型：
       	写法一：const vvalue: string | number
       	写法二：type StringOrNumber = string | number
       	const  value:StringOrNumber 
    2. maybe类型
       表现形式：a:?string
       相当于 a:string | null | void
    3. mixed 和 any 类型
       前者相当于 string | number | ...；还是强类型的，可以通过进行静态检查。
       后者是弱类型的。是为了兼容旧有语法而生。

  flow[手册速查](https://www.saltycrane.com/cheat-sheets/flow-type/latest/)

  [免费科学上网](https://www.eechina.com/thread-129259-1-1.html)

## typescript

![image-20210510131801477](D:\work\日程管理\image-20210510131801477.png)js超集
功能更强大、生态更健全、更完善

ts编译过程：
1.检查类型异常
2.移除类型注解
3.es6+语法降级

## typescript

1. 原始数据类型
2. 一些问题
   - 标准库声明 ：就是内置对象所对应的声明
   - 中文错误提示
   - 作用域问题 全局变量会在不同文件冲突
3. Object类型
4. 数组类型
   - 元组类型 ：明确成员数量和类型的数组
5. 枚举类型
   - 数字枚举
   - 字符串枚举（不常见）
   - 常量枚举
6. 函数类型
7. 任意类型
8. 隐式类型推断
9. 类型断言
   - res as number
   - <number>res
10. 接口
11. 类
    - 4个访问修饰符：public/private/protected/readonly
    - 类接口
    - 抽象类 ：只能被继承，不能实例化
    - 抽象方法：在子类中实现
12. 泛型写法 <T>
13.  类型声明 declare





# js性能优化77