"use strict";
//! 原始数据类型
//
var a = '111'; //只能存字符串
var b = 100; //NaN Infinity
var c = true; //false
// ts中上三种可为空（null或者undefined），严格不行
// const d: boolean = null;
var e = undefined;
//ts中，void类型可无undefined或null(严格只能undefined)
var f = null;
var g = void 0;
var h = Symbol();
//! 标准库声明 ：就是内置对象所对应的声明
console.log(111);
//! 中文错误提示
// yarn tsc --locale zh-CN
// vscode也可设置
//! 作用域问题 全局变量会在不同文件冲突
// 解决方法：放在函数中；或者变成一个es Module
//! Object类型
// object类型不单指普通对象，泛指所有非原始类型。可以是函数、对象、数组
// const foo:object = //[]//{}//function() {}
// 使用字面量方式去单指普通对象
var obj1 = { a: 1, b: 2 };
// 更专业的用法是接口
//! 数组类型
// 和flow一致
//* 方式1：array泛型
var arr1 = [1];
//* 方式2：类似字面量方式
var arr2 = [2];
// ts好处小体会
function sum() {
    var args = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        args[_i] = arguments[_i];
    }
    return args.reduce(function (prev, current) { return prev * current; }, 0);
}
// sum(1, 2, '3');
//! 元组类型 ：明确成员数量和类型的数组
var tuple = [1, '2'];
// 元组应用场景：
Object.entries({
    age: 12,
    name: 'zp',
}); //得到每个键、值都是元组
var mySex = 1 /* man */;
//# sourceMappingURL=test.js.map