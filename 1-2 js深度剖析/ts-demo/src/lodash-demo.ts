import { camelCase } from 'lodash';
import qs from 'query-string';

// 没有声明明确的类型
declare function camelCase(input: string): string;

camelCase('‘hello');

// 类型声明模块

// 有些已经内部集成，如query-string
// qs.stringify({ a: 1 });
