

## vue组件开发

最大程度重用

element-ui

cdd 组件驱动、自下而上

![image-20201208133825921](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208133825921.png)

![image-20201208133839401](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208133839401.png)

### 组件边界情况

- $root  访问根实例成员  
  - 组件状态少的小项目使用方便，复杂项目不建议使用 
- $parent/
  - 子组件可以直接修改父组件成员
  - 简单状态使用
  - 层级太多麻烦，可用依赖注入解决
- $children
  - 访问子组件方法、属性
  - 索引获取，可读性不高，用$ref更好
  - 获取
    - html标签，---  dom对象
    - 子组件 --- 子组件对象

- provide/inject   嵌套多少层都可以这样用，唯一不同的是，注入成员不是响应式的。类似大范围的props
  - 负面影响：组件耦合变高，重构变困难
  - provide 在父组件提供
    - ![image-20201208135253735](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208135253735.png)
  - inject子组件注入
    - ![image-20201208135311257](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208135311257.png)

- ![img](file:///C:\Users\张培\AppData\Local\Temp\SGPicFaceTpBq\13272\0627DE1E.png)**$attrs  父组件中非prop属性绑定到内部组件**
- ![img](file:///C:\Users\张培\AppData\Local\Temp\SGPicFaceTpBq\13272\0628151C.png)**$listeners  父组件中dom对象原生事件绑定到内部组件**
  1. ![image-20201208140217444](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208140217444.png)
  2. ![image-20201208140232167](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208140232167.png)
     1. 使用步骤1：![image-20201208140526734](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208140526734.png)
     2. 使用步骤2：![image-20201208140621460](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208140621460.png)
  3. $listeners 可以展开传递过来的原生事件和自定义事件。

### 快速原型开发

- vuecli  原型快速开发
- 全局扩展   
  - cnpm i -g @vue/cli
  - cnpm i -g @vue/cli-service-global
- vue serve快速查看组件运行效果
  - ![image-20201208141951490](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208141951490.png)

### 结合element

- 安装element-ui
  -  vue add element(小技巧)
    - 省很多操作：安装babel,plugins,配置package.json.
  - ![image-20201208142151218](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208142151218.png)
  - main.js 导入，vue实例渲染看效果

### 步骤条组件

组件分类：第三方组件、基础组件、业务组件

### 表单组件

#### 表单验证	

- 验证模块![image-20201208145428302](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208145428302.png)

- form-item 单独验证
- 表单提交  整体验证

### 维护组件库

单独测试、单独发布、统一管理模块 

![image-20201208150511392](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208150511392.png)

- 源码都在packages目录下

- 固定的目录结构方便管理
  - ![image-20201208150846342](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208150846342.png)

### 开发组件库常用工具

#### storybok

**介绍**

- ![image-20201208151345562](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208151345562.png)

**安装使用**

- ![image-20201208151541511](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208151541511.png)

- 启动 ![image-20201208151634643](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208151634643.png)
- 推荐yarn --- 工作区
- 可以静态部署web服务器

**story怎么编程？**

### yarn工作区

开启

![image-20201208154533954](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208154533954.png)

好处

- 都要下载的依赖可以提升为公共依赖

  

使用

1. ![image-20201208155106256](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208155106256.png)

2. 工作空间安装依赖，然后删除所有局部依赖
3. yarn install ，下载公共依赖，版本不同有局部依赖

### 组件托管工具

**lerna**

- 提交npm,,发布github
- ![image-20201208155338375](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208155338375.png)

**使用**

![image-20201208155409519](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208155409519.png)

- 初始化会创建配置文件lerna.json
- ![image-20201208155659007](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208155659007.png)
- 登录npm,查看用户名
  - ![image-20201208155818819](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208155818819.png)
- 查看当前镜像源
  - ![image-20201208155908062](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208155908062.png)
- 发布 yarn lerna (npm有同名包，会发布失败)

### 组件单元测试

函数输入输出，断言测试。发现组件内可能存在的各种错误。

好处

- ![image-20201208160331463](C:\Users\张培\AppData\Roaming\Typora\typora-user-images\image-20201208160331463.png)