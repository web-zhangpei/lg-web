1. **简述Vue首次渲染过程**
   - 初始化Vue静态成员。比如component、filters等。
   - 初始化Vue生命钩子。
   - 初始化Vue实例成员。比如vm.options、vm.methods
2. **简述Vue响应式原理**
   - 递归的给data中的数据添加getter/setter 
   - 收集依赖dep
   - 数据发生变化，通知wather
   - notify方法去更新视图
3. **简述DOM中key的作用和好处。**
   - 优秀的性能。同样的元素不用重新渲染、不用重绘。
   - diff算法。更快的查找到差异，有针对性更新dom,节省开销。
4. **简述Vue中模板编译的过程。**
   - 把模板转换为AST对象
   - 编译静态节点
   - 编译动态节点和表达式
   - 编译指令

