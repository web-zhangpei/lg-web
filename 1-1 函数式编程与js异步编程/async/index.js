// import './01-promise.js'
// import './02-promise-ajax'
// import './03-promise-lian'
// import './04-promise-catch'
// import './05-promise-static'
// import './01-generator'
// import './02-generator-ajax'
// import './03-generator-recursion'
import MyPromise from './MyPromise/index'
// import MyPromise from './MyPromise'

// const promise1 = new MyPromise((resolve, reject) => {
//   console.log('start1')
//   setTimeout(() => {
//     resolve('end1')
//   }, 2000)
// }).then(value => {
//   console.log(value)
// })

//! 异步逻辑
// const promise2 = new MyPromise((resolve, reject) => {
//   console.log('start2')
//   setTimeout(() => {

//     resolve('end2')
//   }, 1000)
// }).then(value => {
//   console.log(value)
// })

// console.log(MyPromise.resolve()) // 没有静态方法resolve
// console.log(promise.resolve()) // 有实例方法resolve 

//! 返回值是promise对象的情况
// function other () {
//   return new MyPromise((resolve, reject) => {
//     resolve('other')
//   })
// }

// new MyPromise((resolve, reject) => { resolve('aaa') }).then(value => {
//   // console.log('value', value)
//   return other()
// })
//   .then(value => {
//     // console.log('value', value)
//   })

//! 禁止自己返回自己

let promise = new MyPromise((resolve, reject) => { resolve('aaa') })
let p1 = promise.then(value => {
  return p1
})
p1.then(value => {
  console.log('value', value)
}, error => {
  console.log(error)
})

//! 执行器异常
new Promise((resolve, reject) => {
  // throw new Error('executor error')
  resolve('success')
})
  .then(value => {
    throw new Error('then error')

  })
  .catch(err => {
    console.log('err', err)
  })

// ! reject链式调用
new Promise((resolve, reject) => {
  // throw new Error('executor error')
  // resolve('success')
  reject('失败')
})
  .then(value => {
    // throw new Error('then error')

  }, error => {
    return 1000
  })
  .then(value => {
    console.log(value)
  })
  .catch(err => {
    console.log('err', err)
  })