// 1. 普通函数加*
function* foo () {
  console.log('start')

  try {
    const next2 = yield 'yield1'
    next2.done = true
    console.log(next2)

  } catch (error) {

  }
}

// 2. 调用生成器函数，不会立即执行，会返回一个生成器对象
const generator = foo()

/**
 * 3.调用next方法才会开始执行
 * 4.next入参为yield返回值
 * 5.yield入参 为 next返回值
 * 6.yield暂停执行，再次调用next继续执行。
 * 7. done属性来表示是否全部执行完了。
 * 8.generator.throw方法抛出异常
 */
const yield1 = generator.next('next1')
console.log(yield1)
generator.next('next2')

generator.throw(new Error('generator error'))