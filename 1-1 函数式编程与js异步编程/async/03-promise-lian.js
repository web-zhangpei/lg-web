function ajax (url) {
  return new Promise((resolve, reject) => {
    var xhr = new XMLHttpRequest()
    xhr.open('GET', url)
    xhr.responseType = 'json'

    xhr.onload = function () {
      if (this.status === 200) {
        resolve(this.response)
      } else {
        reject(new Error(this.statusText))
      }
    }

    xhr.send()
  })
}

let test = null;
// ! 链式调用
let test2 = ajax('/api/users.json')
  .then((res) => {
    console.log(res)
    return
  })
  .then(res => {
    /**
     * then返回值
     * 1. 非promise普通值，创建一个新promise,把普通值包裹进去。（上一个then返回值  --> 下一个then参数）
     * 2. promise对象，返回该对象承诺后的结果
     */
    test = ajax('/api/posts.json')
    return test
  })
  .then(res => {
    console.log(res)
  })
