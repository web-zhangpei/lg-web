function ajax (url) {
  return new Promise((resolve, reject) => {
    var xhr = new XMLHttpRequest()
    xhr.open('GET', url)
    xhr.responseType = 'json'

    xhr.onload = function () {
      if (this.status === 200) {
        resolve(this.response)
      } else {
        reject(new Error(this.statusText))
      }
    }

    xhr.send()
  })
}
/**
 * 静态方法 vs 实例方法
 * 1. 静态方法由构造函数调用
 * 2. 实例方法由new 出的实例调用
 */


/**
 * 静态方法
 * 1. resolve()
 * 2. reject()
 * 3. all()
 * 4. rece()
 */
// 1.1 resolve()把一个值转化为promise对象
// Promise.resolve('foo').then(val => {
//   console.log(val)
// })

// 等价于

// new Promise((resolve, reject) => {
//   resolve('foo')
// })

// 1.2 resolve包装的promise原样返回

// const promise1 = ajax('/api/posts.json')
// const result1 = Promise.resolve(promise1)
// console.log(promise1 === result1) //true  resolve包装的promise原样返回

// const result2 = new Promise((resolve, reject) => {
//   resolve(promise1)
// })
// console.log(promise1 === result2) //!false  包装进去了内部
/**
 * 1.3 参数对象then方法的方式
 * 为了兼容第三方promise库，将其转换为标准promise(thenable)
 */

// 2. reject()
Promise.reject('失败了').catch(err => {
  // console.log(err)
})


// 3.1 all() 并行执行；链式调用，串行执行
// const allPromise = Promise.all([
//   ajax('./api/posts.json'),
//   ajax('./api/urls.json'),
//   ajax('./api/users.json')
// ])

// allPromise.then((vals) => {
//   // console.log(vals)
// }).catch(err => {
//   console.log(err) //一个失败，全部失败
// })

// 3.2 组合使用串行、并行
// ajax('/api/urls.json')
//   .then(val => {
//     const urls = Object.values(val)
//     const tasks = urls.map(url => ajax(url))
//     return Promise.all(tasks)
//   })
//   .then(vals => {
//     // console.log(vals)
//   })
//   .catch(err => {
//     console.log(err)
//   })

/**
 * 4. race()一个任务完成就结束 
 * rece 比速度
 * 用途：常用来统一控制ajax超时请求。所有请求都不能超过设置的时间。
 * 场景：不希望用户等三分钟以上，超过三分钟提示用户网络有问题。
 */
const ajax1 = ajax('/api/posts.json')
const timeout = new Promise((resolve, reject) => {
  setTimeout(() => reject(new Error('请求超时，请检查网络')), 500)
})

Promise.race([
  ajax1,
  timeout
])
  .then(val => {
    console.log(val)
  })
  .catch(err => {
    console.log(err)
  })


