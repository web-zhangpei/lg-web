function ajax (url) {
  return new Promise((resolve, reject) => {
    var xhr = new XMLHttpRequest()
    xhr.open('GET', url)
    xhr.responseType = 'json'

    xhr.onload = function () {
      if (this.status === 200) {
        resolve(this.response)
      } else {
        reject(new Error(this.statusText))
      }
    }

    xhr.send()
  })
}

function* main () {
  const users = yield ajax('/api/users.json')
  console.log(users)
  const posts = yield ajax('/api/posts.json')
  console.log(posts)
}

const g = main()
// 开始到yield结束
const result1 = g.next()

result1.value.then(data => {
  // yield继续到第二个yield结束
  const result2 = g.next(data)
  result2.value.then(data => {
    g.next(data)
  })
})