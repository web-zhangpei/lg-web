function ajax (url) {
  return new Promise((resolve, reject) => {
    var xhr = new XMLHttpRequest()
    xhr.open('GET', url)
    xhr.responseType = 'json'

    xhr.onload = function () {
      if (this.status === 200) {
        resolve(this.response)
      } else {
        reject(new Error(this.statusText))
      }
    }

    xhr.send()
  })
}

function* main () {
  try {
    const users = yield ajax('/api/users.json')
    console.log(users)
    const posts = yield ajax('/api/posts.json')
    console.log(posts)
    const posts1 = yield ajax('/api/posts1.json')
    console.log(posts1)
  } catch (error) {
    console.log(error)
  }
}

// https://github.com/ti/co
co(main)

function co (generator) {
  const g = generator()

  function handleResult (result) {
    if (result.done) return
    result.value.then(data => {
      handleResult(g.next(data))
    }, err => {
      g.throw(err)
    })
  }
  handleResult(g.next())
}
