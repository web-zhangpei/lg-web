function ajax (url) {
  return new Promise((resolve, reject) => {
    var xhr = new XMLHttpRequest()
    xhr.open('GET', url)
    xhr.responseType = 'json'

    xhr.onload = function () {
      if (this.status === 200) {
        resolve(this.response)
      } else {
        reject(new Error(this.statusText))
      }
    }

    xhr.send()
  })
}

let test = null;
// ! 链式调用
let test2 = ajax('/api/users1.json')
  .then((res) => {
    console.log(res)
    return
  })
  .then(res => {
    test = ajax('/api/posts1.json')
    return test
  })


window.addEventListener('unhandledrejection', evt => {
  const { reason, promise } = evt
  console.log(reason, promise)
  evt.preventDefault()
})