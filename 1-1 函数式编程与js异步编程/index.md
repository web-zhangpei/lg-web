## 1-1-2 异步编程

单线程模式js的异步方案
dom操作必须单线程
执行代码的线程只有一个

- 同步模式
- 异步模式
  - 异步如何实现：事件循环与消息队列
  - 异步编程的几种方法
  -  promise异步方案、宏任务微任务队列
  - generator异步方案（2015）、async/await语法糖（2017）

### 同步模式

调用栈（js引擎维护的 正在执行的工作表）

- 记录正在做的事情（函数的调用，函数的声明不会压入调用栈）

### 异步模式

- 同步模式简单，有些耗时操作需要异步执行（ajax、node中文件读写），避免代码卡死
- 非常重要的模式，可以同时处理大量耗时任务

js执行过程中的机制与环境：
1  web APIs(运行环境的内部api)
2  事件循环（event loop）
3 消息队列（又称回调队列）(queue)

事件循环只有一个作用：监听调用栈、消息队列。调用栈结束，从消息队列（回调）中取回调函数
调用栈是后入先出，方便记录函数的执行顺序。
js是单线程。浏览器不是单线程。

![image-20210505092635343](\images\image-20210505092635343.png)

### 回调函数

所有异步编程方案本质、根基是回调函数
除了传递回调函数，还有几种如事件机制、发布定义。也是基于回调函数的变体。

### Promise

传统的回调函数，造成回调函数嵌套（回调地狱）。

![image-20210505093332251](D:\Users\张培\Desktop\gitee\lg-web\1 函数式编程与js异步编程\images\image-20210505093332251.png)

承诺后的结果不可更改。只能调用二个结果其一。
promise执行的时序问题特殊，后面说。

#### 链式调用

then()都返回一个全新的promise，好处是每个承诺都不影响，promise都是基于上一个承诺的结果，接力赛。

![image-20210505105029594](D:\Users\张培\Desktop\gitee\lg-web\1 函数式编程与js异步编程\images\image-20210505105029594.png)

#### 异常处理

##### 1. catch是then的别名，即then(undefind,fn)

catch和rejectFn的区别：
rejectFn就是then(resolveFn,rejectFn)，它只能捕获上一个promise的异常
catch可以捕获整个链条的异常，因为异常会被向后传递。

unhandledrejection

##### 2.捕获全局异常（不推荐）

应该在代码中明确的捕获每一个异常

```js
window.addEventListener('unhandledrejection', evt => {
  const { reason, promise } = evt
  console.log(reason, promise)
  evt.preventDefault()
})
```

nodejs中

![image-20210505213421911](D:\Users\张培\Desktop\gitee\lg-web\1 函数式编程与js异步编程\images\image-20210505213421911.png)

##### 3.执行时序

大部分异步调用都是作为宏任务执行，如setTimeout()
promise/mutationObserver/process.nextTick是作为微任务执行。
宏任务：回调队列中的任务。宏任务执行过程中临时加的额外需求任务，可以作为新的宏任务去队列排队，也可以作为当前宏任务的微任务。
微任务：当前宏任务的微任务。它的引入是为了执行完主要任务，还有一些小任务不用重新排队，提高执行效率。

### Generator

### Async/Await

语法糖，语言层面的异步编程标准

async返回一个promise，方便对代码整体控制

## 1-1-3 手写promise源码

- promise类核心逻辑实现
- 加入异步逻辑
- 实现then方法多次调用
- 实现then()方法链式调用